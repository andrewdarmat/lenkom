$(document).ready(function(){
  var regionsDrop = $('#regions'),
    regionsSelected = $('#regions-selected'),
    menu = $('#regions-menu');
  regionsDrop.find('.regions__link').click(function(e){
    e.preventDefault();
    var item = $(this).parent();
    if(!item.hasClass('regions__item--active')){
      regionsDrop.find('.regions__item').removeClass('regions__item--active');
      item.addClass('regions__item--active');
      regionsSelected.text(item.text());
      $('.regions__selected').removeClass('regions__selected--active');
      menu.slideUp('fast');
    }
  });

  $('.regions__selected').click(function(){
    $(this).toggleClass('regions__selected--active');
    menu.slideToggle('fast');
  });
});

$(document).click(function(e){
  let div = $('#regions-menu,.regions__selected');

  if( !div.is(e.target) && div.has(e.target).length < 1 ){
    $('#regions-menu').hide(0);
    $('.regions__selected').removeClass('regions__selected--active');
  }
});
