$('.header__btn-menu').on('click', function (e) {
  $(this).toggleClass('header__btn-menu--active');
  $('.header__menu-wrap').slideToggle(300);
  $(this).parent().parent().parent().toggleClass('open');
});