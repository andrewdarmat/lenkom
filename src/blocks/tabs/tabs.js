$(document).on('click','.tabs__link',function(e){
  e.preventDefault();
  var link = $(this),
      menu = link.parents('.tabs__menu'),
      tabsWrap = menu.parents('.tabs').find('.tabs__content');
  if(!link.hasClass('tabs__link--active')){
    menu.find('.tabs__link').removeClass('tabs__link--active');
    tabsWrap.find('.tabs__item--active').hide(0).removeClass('tabs__item--active');
    link.addClass('tabs__link--active');
    $(link.attr('href')).fadeIn('fast',function(){
      $(this).addClass('tabs__item--active')
    })
  }
});